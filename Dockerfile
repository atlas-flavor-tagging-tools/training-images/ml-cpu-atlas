FROM atlasml/ml-base:latest

## ensure locale is set during build
ENV LANG C.UTF-8

RUN pip3 install jupyterlab --user && \
    pip3 install seaborn --user && \
    pip3 install xgboost --user && \
    pip3 install tables --user && \
    pip3 install hep_ml  --user

# RUN apt-get update && apt-get install -y nano
