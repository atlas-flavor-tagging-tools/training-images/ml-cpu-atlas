# ML-Container

This is an exemplary container for machine learning purposes on CPUs based on the [ATLAS ml-containers](https://gitlab.cern.ch/aml/containers/docker)

to run this image with singularity run the command for CPU optimised container
```
singularity exec --contain docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-cpu-atlas/ml-cpu-atlas:latest bash
```



In case you run into troubles that singularity is using your local software (e.g. anaconda) you can change your home e.g. like
```
 -C --home /tmp
```
with `-B` you can add other directories to the container of needed.